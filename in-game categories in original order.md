1. Essentials - Manual, Workbench, Hunter
1. Supplies - Manual, Workbench, Hunter, Farmer
1. Ammo - Alchemist, Hunter
1. Production Place - Blacksmith, Alchemist, Carpenter, Hunter, Farmer
1. Resources - Manual, Workbench, Blacksmith, Alchemist, Carpenter, Hunter, Farmer
1. Survival - Manual, Workbench, Blacksmith, Carpenter, Hunter
1. Weapons - Manual, Workbench, Blacksmith, Alchemist
1. Magical Weapons - Manual, Workbench, Alchemist
1. Armor - Manual, Workbench, Blacksmith, Alchemist, Hunter
1. Comfort - Carpenter, Hunter
1. Decorative - Blacksmith, Alchemist, Carpenter, Farmer
1. Tools & Components - Blacksmith, Alchemist, Carpenter, Hunter, Farmer
