Whole map:

|     |Lat-Y |Lon-X |
|-----|------|------|
| SW: |    0 |    0 |
| SE: |    0 | 8191 |
| NW: | 8191 |    0 |
| NE: | 8191 | 8191 |


Early access area corners with ingame coordiantes:

|     |Lat-Y |Lon-X |Height-Z |
|-----|------|------|------|
| SW: |   50 | 1590 |  863 |
| SE: |   50 | 8141 |  697 |
| NW: | 3495 | 1590 | 1221 |
| NE: | 3495 | 8141 | 1211 |


8191,8191 = size of whole map
3450,6550 - 50,1590 = 3400,4960 = size of EA area

3400*4960 / 8191*8191 = 25.14%
